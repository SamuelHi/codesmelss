import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MovieTest {

    public Movie testMovie;
    @BeforeEach
    void setUp() {
        testMovie = new Movie("Der Drachenlord",2);
    }

    @Test
    void getPriceCode() {
        assertEquals(2,testMovie.getPriceCode());
    }

    @Test
    void setPriceCode() {
        testMovie.setPriceCode(1);
        assertEquals(1,testMovie.getPriceCode());
    }

    @Test
    void getTitle() {
        assertEquals("Der Drachenlord",testMovie.getTitle());
    }
}