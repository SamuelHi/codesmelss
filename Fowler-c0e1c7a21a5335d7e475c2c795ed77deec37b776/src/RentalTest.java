import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RentalTest {

    Rental testRental;
    Movie testMovie;
    @BeforeEach
    void setUp() {
        testMovie = new Movie("Der Drachenlord",1);
        testRental = new Rental(testMovie,4);
    }

    @Test
    void getDaysRented() {
        assertEquals(4,testRental.getDaysRented());
    }

    @Test
    void getMovie() {
        assertEquals(testMovie,testRental.getMovie());
    }

    @Test
    void getFrequentRenterPoints(){

        assertEquals(2,testRental.getFrequentRenterPoints());
    }


}