import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {


    Customer testCustomer;

    @BeforeEach
    void setUp(){
        testCustomer = new Customer("Rainer");
    }



    @org.junit.jupiter.api.Test
    void getName() {
        assertEquals( "Rainer",testCustomer.getName());
    }

    @org.junit.jupiter.api.Test
    void statement() {

        String outputofStatement = "Rental Record for Rainer" + "\n";
        outputofStatement += "\t" + "Title" + "\t" + "\t" + "Days" + "\t" + "Amount" + "\n";
        outputofStatement += "Amount owed is " + "0.0" + "\n";
        outputofStatement += "You earned " + "0" + " frequent renter points";

        assertEquals(outputofStatement, testCustomer.statement());

    }


}