# Codesmells for the Commits, cause i missed that the commits should be named by the Codesmell they fixed

1. 72993db6 - Adding of Testfile to see if the programm works after refactoring
2. 49449a1d - changed names to give a clearer meaning to them 
3. 73fbd283 - moved Method to the right class cause it just used values of another class
4. 0c67ccb6 - removed temporary variable, cause they create a lot of parameters to be passed around
5. d662f875 - same  as 3
6. 40d7a68a - same as 1, test for new method
7. 4b1d05d9 - removing temporary variables by using return methods
8. 2728e3b3 - same as 7
9. bff1db91 - same as 6
10. 0d04e92d - same as 3
11. c80d1af1 - Replacing the Conditional Logic on Price Code with Polymorphism
12. 984c6ea3 - same as 11
13. 8233fa7a - same as 11

A big part of the commits did the Decomposing and Redistributing the Statement Method

A feature for refactoring that my used IDE Intellij provides is the the use of the Refactoring tab. 
It has many options to choose from. One for Example is called "Replace temp with query", which can be used on a temporary variable to create
a return method for it that replaces the variable.
